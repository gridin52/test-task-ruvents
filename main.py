import calendar
import openpyxl
from datetime import datetime
from os import path


def get_worksheet(file_path: str):
    """
    Читаем XLSX файл и возвращает раздел TASKS из файла.
    :param file_path: Полный путь до файла XLSX
    :return: Экземпляр класса openpyxl c данными
    """
    xlsx_file = openpyxl.load_workbook(file_path)  # Загружаем файл
    sheet_name = xlsx_file.sheetnames[1]  # Выбираем раздел Tasks
    worksheet = xlsx_file[sheet_name]
    return worksheet

def get_values_from_column(sheet, column_num: int) -> list:
    """
    Читаем все строки в указанной колонке.
    :param sheet: Экземпляр класса openpyxl c данными.
    :param column_num: Номер колонки.
    :return: Значения колонок.
    """
    for row in sheet.iter_rows(min_col=column_num, max_col=column_num, min_row=3, max_row=sheet.max_row):
        yield row[0].value

    """ Можно возвращать и return, но если записей будет 2 миллиона, то так быстрей потому, что в других функциях мы не ждем наполнения словаря, а проверяем сразу значения.
    Например так:
    return [row[0].value  for row in sheet.iter_rows(min_col=column_num, max_col=column_num, min_row=3, max_row=sheet.max_row)]
    """

def get_the_count_of_even_numbers(sheet) -> int:
    """
    Возвращает количество четных чисел в колонке.
    :param sheet: Экземпляр класса openpyxl c данными.
    :return: Количество четных чисел.
    """
    count = 0

    for row in get_values_from_column(sheet, 2):
        if isinstance(row, int) and row % 2 == 0:
            count += 1
    return count

def get_the_count_of_prime_numbers(sheet) -> int:
    """
    Возвращает количество простых чисел в колонке.
    :param sheet: Экземпляр класса openpyxl c данными.
    :return: Количество простых чисел.
    """
    def __is_prime(num):
        if isinstance(num, int) is False: # Проверяем, что это в принципе целое число.
            return False

        value = abs(num) # Получаем значение по модулю

        if value == 1:
            return False

        for i in range(2, int(value**0.5)):
            if value % i == 0:
                return False
        return True

    count = 0

    for row in get_values_from_column(sheet, 3):
        if __is_prime(row):
            count += 1
    return count

def get_floats_count_less(sheet, less_than: float) -> int:
    """
    Возвращает количество чисел, которое меньше числа, указанного в less_than.
    :param sheet: Экземпляр класса openpyxl c данными.
    :param less_than: Число, с которым сравниваем.
    :return: Количество числе, меньших числа less_than.
    """
    count = 0

    for row in get_values_from_column(sheet, 4):
        # Здесь реализована проверка на тип данных с вызовом Exception. По усу, так нужно было сделать в каждой функции.
        if isinstance(row, (str, int, float)):
            if isinstance(row, str):
                row = row.replace(' ', '') # Удаляем пробелы, которые есть в сроках
                row = row.replace(',', '.') # Заменяем запятые на точки

                for char in row: # Проверяем, что в строке нет буквенных символов
                    if char.isalpha():
                        raise TypeError(f"Значение невозможно конвертировать в FLOAT. В строке содержится символ {char}")
            float_num = float(row)

            if float_num < less_than:
                count += 1
        else:
            raise TypeError(f"Значение невозможно конвертировать в FLOAT. Значение: {row}, тип {type(row)}")
    return count

def get_count_of_tue_by_name(sheet, day: str = 'tue') -> int: # Аргумент day тут стоит для того, чтобы можно было проверять и другие дни, если понадобится. Имя функции только надо поменять))))
    """
    Возвращает количество дней, указанных в day, которые есть в колонке.
    :param sheet: Экземпляр класса openpyxl c данными.
    :param day: День, который мы ищем в формате mon, tue, wed, thu, fri, sat, sun.
    :return: Количество дней.
    """
    count = 0

    for row in get_values_from_column(sheet, 5):
        if day.lower() in row.lower():
            count += 1
    return count

def get_tue_by_datetime(sheet) -> int:
    """
    Возвращает количество вторников в колонке.
    :param sheet: Экземпляр класса openpyxl c данными.
    :return: Количество вторников.
    """
    count = 0

    for row in get_values_from_column(sheet, 6):
        datetime_object = datetime.strptime(row, "%Y-%m-%d %H:%M:%S.%f")
        if datetime_object.weekday() == 1:
            count += 1
    return count

def get_last_tuesday_of_month(sheet):
    """
    Возвращает количество последних вторников в месяце, указанных в колонке
    :param sheet: Экземпляр класса openpyxl c данными.
    :return: Количество вторников.
    """
    count = 0

    # Для реализации этой функции, я бы использовал модуль calendar
    for row in get_values_from_column(sheet, 7):
        datetime_object = datetime.strptime(row, "%m-%d-%Y")
        last_tuesday = max(week[1] for week in calendar.monthcalendar(datetime_object.year, datetime_object.month))
        if datetime_object == datetime.strptime(f'{datetime_object.month}-{last_tuesday}-{datetime_object.year}', '%m-%d-%Y'):
            count += 1
    return count

if __name__ == '__main__':
    xlsx_file_path = path.join(path.dirname(__file__), 'task_support.xlsx') # Получаем абсолютный путь до файла xlsx

    worksheet = get_worksheet(xlsx_file_path)

    count_of_even_nums = get_the_count_of_even_numbers(worksheet)
    print(f'Количество четных чисел: {count_of_even_nums}')

    count_of_prime_nums = get_the_count_of_prime_numbers(worksheet)
    print(f'Количество простых чисел: {count_of_even_nums}')

    count_less_then = get_floats_count_less(worksheet, 0.5)
    print(f'Количество чисел меньше 0.5: {count_less_then}')

    tuesday_count_by_name = get_count_of_tue_by_name(worksheet)
    print(f'Количество вторников по имени: {tuesday_count_by_name}')

    tuesday_count_by_datetime = get_tue_by_datetime(worksheet)
    print(f'Количество вторников в datetime формате: {tuesday_count_by_datetime}')

    last_tuesday_in_month = get_last_tuesday_of_month(worksheet)
    print(f'Количество последних вторников в месяце: {last_tuesday_in_month}')

